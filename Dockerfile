FROM alpine:latest

WORKDIR /app

ADD app /app/app

ENTRYPOINT ["/app/app"]