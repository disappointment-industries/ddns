package main

import (
	"bufio"
	"fmt"
	"gitlab.com/MikeTTh/env"
	"io/ioutil"
	"net/http"
	"time"
)

var Ip string

type Domain struct {
	Host     string
	Domain   string
	Password string
}

func (d *Domain) Update(ip string) {
	resp, err := http.Get(fmt.Sprintf("https://dynamicdns.park-your-domain.com/update?host=%s&domain=%s&password=%s&ip=%s", d.Host, d.Domain, d.Password, ip))
	if err != nil {
		fmt.Printf("Couldn't update %s to %s\n", d.Host+"."+d.Domain, ip)
		fmt.Println(err)
	} else {
		fmt.Printf("Updated %s to %s\n", d.Host+"."+d.Domain, ip)
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("msg:", string(b))
	}
}

var dom = env.String("DOMAIN", "")
var pass = env.String("PASSWORD", "")
var host = env.String("HOST", "")

func main() {
	var domains []Domain
	domains = append(domains, Domain{host, dom, pass})

	for true {
		ipr, err := http.Get("http://checkip.amazonaws.com/")
		if err == nil {
			s := bufio.NewScanner(ipr.Body)
			prevIp := Ip
			Ip = ""
			for s.Scan() {
				Ip += s.Text()
			}
			if Ip != prevIp {
				for _, d := range domains {
					d.Update(Ip)
				}
			}
		} else {
			fmt.Println("Couldn't get ip")
		}
		time.Sleep(10 * time.Minute)
	}

}
